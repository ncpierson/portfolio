#! /bin/bash

# Handle PNG images

for PHOTO in src/assets/raw/*.png; do
  NAME=`basename $PHOTO .png`;
  convert "$PHOTO" -strip -resize 480x -sharpen 0x.5 "src/assets/img/${NAME}-480.png"
  convert "$PHOTO" -strip -resize 960x -sharpen 0x.5 "src/assets/img/${NAME}-960.png"
  convert "$PHOTO" -strip -resize 1920x "src/assets/img/${NAME}-1920.png"
done

pngquant -f --ext .png src/assets/img/*.png

# Handle JPG images

for PHOTO in src/assets/raw/*.jpg; do
  NAME=`basename $PHOTO .jpg`;
  convert "$PHOTO" -sampling-factor 4:2:0 -strip -resize 480x -sharpen 0x.5 -quality 75 "src/assets/img/${NAME}-480.jpg"
  convert "$PHOTO" -sampling-factor 4:2:0 -strip -resize 960x -sharpen 0x.5 -quality 75 "src/assets/img/${NAME}-960.jpg"
  convert "$PHOTO" -sampling-factor 4:2:0 -strip -resize 1920x -quality 75 "src/assets/img/${NAME}-1920.jpg"
done

# Handle SVG images

cp src/assets/raw/*.svg src/assets/img/

# Handle icons

bash scripts/icons.sh
