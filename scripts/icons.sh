#! /bin/bash

# Manifest Icons

convert 'src/assets/raw/favicon.png' -strip -resize 512x 'src/assets/icons/android-chrome-512x512.png'
convert 'src/assets/raw/favicon.png' -strip -resize 192x 'src/assets/icons/android-chrome-192x192.png'
convert 'src/assets/raw/favicon.png' -strip -resize 180x 'src/assets/icons/apple-touch-icon.png'
convert 'src/assets/raw/favicon.png' -strip -resize 150x 'src/assets/icons/mstile-150x150.png'
convert 'src/assets/raw/favicon.png' -strip -resize 32x 'src/assets/icons/favicon-32x32.png'
convert 'src/assets/raw/favicon.png' -strip -resize 16x 'src/assets/icons/favicon-16x16.png'

# Root Favicon

convert 'src/assets/raw/favicon.png' -define icon:auto-resize=48,32,16 'src/assets/favicon.ico';
