#! /bin/bash

source ~/.nvm/nvm.sh

cd ~/Deployment

rm -rf portfolio

git clone git@gitlab.com:ncpierson/portfolio.git

cd portfolio

yarn

yarn images

yarn build

cp -R build/* /var/www/portfolio.nick.exposed
