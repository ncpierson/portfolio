import App from './components/app';

import './style/index.css';
import './style/section.css';
import './style/grid.css';
import './style/hero.css';
import './style/project.css';
import './style/button.css';

export default App;
