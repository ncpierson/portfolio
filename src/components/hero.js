import { h, Component } from 'preact';

function Hero({ title, image, suffix, client, roles }) {
  const $roles = roles.map(role => <li class="tag">{role}</li>);

  return (
    <header class="hero">
      <div class="hero__image">
        <img
          alt=""
          srcset={`
            /assets/img/${image}-480.${suffix} 480w,
            /assets/img/${image}-960.${suffix} 960w,
            /assets/img/${image}-1920.${suffix} 1920w,
          `}
          src={`/assets/img/${image}-960.${suffix}`}
          sizes="100vw"
        />
      </div>
      <div class="hero__body">
        <h1 class="hero__title">{title}</h1>
        <div class="info">
          <span class="label">for</span>
          <ul class="tags">
            <li class="tag">{client}</li>
          </ul>
        </div>
        <div class="info">
          <span class="label">doing</span>
          <ul class="tags">{$roles}</ul>
        </div>
      </div>
    </header>
  );
}

export default Hero;
