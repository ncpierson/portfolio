import { h, Component } from 'preact';
import { Router } from 'preact-router';
import ReactGA from 'react-ga';

import Home from 'async!../routes/home';
import Resume from 'async!../routes/resume';
import SoundOfText from 'async!../routes/soundoftext';
import BloxelsEdu from 'async!../routes/bloxels-edu';
import BloxelsShopify from 'async!../routes/bloxels-shopify';
import KaitlynClinkPhotography from 'async!../routes/kaitlynclink';
import Portfolio from 'async!../routes/portfolio';

export default class App extends Component {
  constructor() {
    super();

    if (
      typeof window !== 'undefined' &&
      window.location.host === 'nickpierson.name'
    ) {
      ReactGA.initialize('UA-101624095-4');
    }
  }

  /** Gets fired when the route changes.
   *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
   *	@param {string} event.url	The newly routed URL
   */
  handleRoute = e => {
    ReactGA.pageview(e.url);

    this.currentUrl = e.url;
  };

  render() {
    return (
      <div id="app">
        <Router onChange={this.handleRoute}>
          <Home path="/" />
          <Resume path="/resume" />
          <SoundOfText path="/soundoftext" />
          <BloxelsEdu path="/bloxels-edu" />
          <BloxelsShopify path="/bloxels-shopify" />
          <KaitlynClinkPhotography path="/kaitlynclink" />
          <Portfolio path="/portfolio" />
        </Router>
      </div>
    );
  }
}
