import { h, Component } from 'preact';

class Piece extends Component {
  render() {
    const { image, suffix, alt, label, children, center, reverse } = this.props;

    return (
      <div class={'piece' + (reverse ? ' piece--reverse' : '')}>
        <div class="piece__text">{children}</div>
        <div class={'piece__image' + (center ? ' piece__image--center' : '')}>
          {label && <span class="piece__image-label">{label}</span>}
          <img
            alt={alt}
            srcset={`
              /assets/img/${image}-480.${suffix} 480w,
              /assets/img/${image}-960.${suffix} 960w,
              /assets/img/${image}-1920.${suffix} 1920w,
            `}
            sizes={`
              (min-width: 1280px) 524px,
              (min-width: 600px) 50vw,
              100vw
            `}
            src={`/assets/img/${image}-960.${suffix}`}
          />
        </div>
      </div>
    );
  }
}

export default Piece;
