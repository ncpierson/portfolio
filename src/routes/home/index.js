import { h, Component } from 'preact';
import { Link } from 'preact-router/match';

class Home extends Component {
  render() {
    return (
      <div id="#home">
        <About />
        <Work />
        <Contact />
      </div>
    );
  }
}

class About extends Component {
  render() {
    return (
      <section id="about" class="section">
        <div class="section__image">
          <img
            src="/assets/img/deskman.svg"
            alt="Stick man hunched over desk while working"
          />
        </div>
        <div class="section__body">
          <h1 class="section__title">Hi, I'm Nick.</h1>
          <p class="section__text">
            I am a <strong>full-stack web developer</strong>. I am currently
            most familiar with NodeJS, Express, and React — however I have
            learned many different languages and frameworks during my career and
            I am eager to keep learning. I emphasize respect, kindness, and
            gentle feedback in my relationships with other humans.
          </p>
          <p class="section__text">
            If you want to know more about me than the projects I have done, I
            recommend checking out <a href="//blog.nickpierson.name">my blog</a>.
          </p>
        </div>
      </section>
    );
  }
}

class Work extends Component {
  render() {
    return (
      <section id="work" class="section section--colored">
        <div class="grid">
          <WorkItem
            label="Sound of Text"
            name="soundoftext"
            suffix="png"
            alt="Screenshot of Sound of Text"
          />
          <WorkItem
            label="Bloxels Edu"
            name="bloxels-edu"
            suffix="jpg"
            alt="Screenshot of Bloxels Edu home page"
          />
          <WorkItem
            label="Bloxels Shopify"
            name="bloxels-shopify"
            suffix="png"
            alt="Screenshot of Bloxels Shopify"
          />
          <WorkItem
            label="Kaitlyn Clink Photography"
            name="kaitlynclink"
            suffix="jpg"
            alt="Screenshot of Kaitlyn Clink Photography"
          />
          <WorkItem
            label="This Site"
            name="portfolio"
            suffix="png"
            alt="Screenshot of this site containing a screenshot of this site..."
          />
        </div>
      </section>
    );
  }
}

function WorkItem(props) {
  const { label, name, suffix, alt } = props;

  return (
    <Link href={`/${name}`} class="grid__item">
      <img
        alt={alt}
        class="grid__image"
        src={`/assets/img/${name}-960.${suffix}`}
        srcset={`
          /assets/img/${name}-480.${suffix} 480w,
          /assets/img/${name}-960.${suffix} 960w,
          /assets/img/${name}-1920.${suffix} 1920w
        `}
        sizes={`
          (min-width: 1280px) 340px,
          (min-width: 960px) 33vw,
          (min-width: 600px) 50vw,
          100vw
        `}
      />
      <div class="grid__label">{label}</div>
    </Link>
  );
}

class Contact extends Component {
  render() {
    return (
      <section id="contact" class="section section--reverse">
        <div class="section__image">
          <img src="/assets/img/byeman.svg" alt="Stick man waving" />
        </div>
        <div class="section__body">
          <h2 class="section__title">Email me.</h2>
          <p class="section__text">
            Have an opportunity for me that I might be interested in? I would
            love to hear from you.
          </p>
          <a href="mailto:nick@nickpierson.name" class="button">
            Email me
          </a>
        </div>
      </section>
    );
  }
}

export default Home;
