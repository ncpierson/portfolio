import { h, Component } from 'preact';

import Hero from '../../components/hero';
import Piece from '../../components/piece';

const roles = ['development', 'design', 'hosting'];

class Portfolio extends Component {
  render() {
    return (
      <div id="#meta">
        <Hero
          title="Nick Pierson's Portfolio"
          image="portfolio"
          suffix="png"
          client="myself"
          roles={roles}
        />
        <main class="project">
          <Piece image="portfolio" suffix="png" alt="">
            <p>
              <a href="/">Strange loops</a> are one of my favorite things, so it
              made sense to me to talk about this very website on... itself. I
              was contracted by me to build myself a portfolio. The goal with
              the design was to communicate that I am a competent and
              established web developer. Personally, I quite dislike résumés and
              I wanted something that I could point to as a replacement.
            </p>
            <p>
              I have been designing interfaces since I was building Android
              applications in college, but only recently have I taken design
              more seriously. For this website,{' '}
              <strong>I decided to learn Figma</strong>, which is an application
              quite similar to Sketch, except that it works on platforms other
              than Mac. I had never designed using Figma before, but I managed
              to come up with the final high-fidelity mock-ups for this site
              before starting on development. I used to write the content in
              HTML and then design the site in my browser, so doing a full
              design before development was quite the change to my workflow.
            </p>
          </Piece>
          <Piece image="portfolio-project" suffix="jpg" alt="" reverse>
            <p>
              Behind the scenes, I wanted a lightweight technology stack in
              order to avoid unnecessary overhead to page load. I was originally
              going to go with Gulp, but as I have spent more time with React, I
              find Gulp to require too much tinkering. The problem is that React
              is about 40kb when gzipped, which is not much, but entirely
              unnecessary since I really only wanted the toolchain behind Create
              React App, and not the <em>reactiveness</em> of React.
            </p>
            <p>
              After{' '}
              <a href="https://twitter.com/NickOnTheWeb/status/935971488838115328">
                some Twitter recommendations
              </a>, I ultimately ended up choosing{' '}
              <strong>Preact, using the Preact CLI tool</strong>. Preact is
              still overkill for a mostly static site, but at 4kb gzipped, the
              overhead is almost nothing. So far it has been incredibly easy to
              use and I am glad I made the choice.
            </p>
          </Piece>
          <Piece image="portfolio-sot" suffix="jpg" alt="">
            This site is hosted using Nginx on the same DigitalOcean server as
            my blog. I have automated deployment setup using Gitlab CI. This
            website is open source and you can see the code{' '}
            <a href="https://gitlab.com/ncpierson/portfolio">on Gitlab</a>.
          </Piece>
        </main>
      </div>
    );
  }
}

export default Portfolio;
