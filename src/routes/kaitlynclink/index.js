import { h, Component } from 'preact';

import Hero from '../../components/hero';
import Piece from '../../components/piece';

const roles = ['development', 'design', 'hosting'];

class KaitlynClinkPhotography extends Component {
  render() {
    return (
      <div id="#kaitlynclink">
        <Hero
          title="Kaitlyn Clink Photography"
          image="kaitlynclink"
          suffix="jpg"
          client="Kaitlyn Clink"
          roles={roles}
        />
        <main class="project">
          <Piece image="kaitlynclink" suffix="jpg" alt="">
            <p>
              My cousin, Kaitlyn, is running her own photography business. She
              had a website on Weebly and I offered to replace it with a
              custom-designed website with her own domain for the low cost of
              paying for hosting. She eagerly accepted and her online portfolio
              is now at <a href="http://kaitlynclink.com">kaitlynclink.com</a>.
            </p>
            <p>
              The goal for the design was to showcase her work as a
              photographer, share about her experience, and give people an
              opportunity to reach out to her more easily. The overall design
              and color palette were chosen by Kaitlyn after a round of ideation
              and feedback. This process allowed Kaitlyn to end up with{' '}
              <strong>a website that she loves</strong>.
            </p>
          </Piece>
          <Piece image="kaitlynclink-gallery" suffix="jpg" alt="" reverse>
            <p>
              An important piece of this project, and any photographer
              portfolio, is the time it takes for the page to load. It is a
              balance between high-quality photos and faster load times. This
              project was one of the first that{' '}
              <strong>
                allowed me to really dive into responsive images and image
                compression techniques
              </strong>. On mobile-screens, the home page of kaitlynclink.com
              can finish loading in under 3 seconds, even while on 3G internet
              speeds.
            </p>
          </Piece>
          <Piece image="kaitlynclink-about" suffix="jpg" alt="">
            <p>
              Behind the scenes, I used <strong>Pug templates and SASS</strong>.
              I also used <strong>Gulp and Webpack</strong> for builds, file
              bundling, and image compression. The site itself is static and
              hosted with Apache on a DigitalOcean server.
            </p>
          </Piece>
        </main>
      </div>
    );
  }
}

export default KaitlynClinkPhotography;
