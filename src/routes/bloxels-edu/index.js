import { h, Component } from 'preact';

import Hero from '../../components/hero';
import Piece from '../../components/piece';

const roles = ['development'];

class BloxelsEdu extends Component {
  render() {
    return (
      <div id="#bloxels-edu">
        <Hero
          title="Bloxels EDU"
          image="bloxels-edu"
          suffix="jpg"
          client="Pixel Press"
          roles={roles}
        />
        <main class="project">
          <Piece
            image="bloxels-edu"
            suffix="jpg"
            alt="Screenshot of above the fold content"
          >
            <p>
              The{' '}
              <a href="http://edu.bloxelsbuilder.com">Bloxels EDU website</a> is
              the online front for the education-related portion of the Bloxels
              brand — owned by{' '}
              <a href="http://www.projectpixelpress.com">
                Pixel Press Technology
              </a>. It is a mobile-friendly, single-page site measuring well
              over 20 mouse wheel scrolls in length. The design for the website
              was already prepared when I was contracted, and my responsibility
              was to bring it to life.
            </p>
            <p>
              I was able to work closely with the designer to get feedback and
              allow quick iteration on the design in places that were unclear. I
              also worked closely with an internal developer to make any
              technical decisions since I would not be in the position to
              maintain the site down the road. I built the site with{' '}
              <strong>plain HTML and SCSS</strong>. I used{' '}
              <strong>Backbone.js</strong> in the few places JavaScript was
              needed because both the internal developer and I had experience
              with it.
            </p>
          </Piece>
          <Piece
            image="bloxels-edu-howitworks"
            suffix="png"
            alt="Screenshot of how bloxels works"
            reverse
          >
            It was important to <strong>focus on performance</strong> since the
            Bloxels EDU page contains so much rich media. The largest
            performance hit was the focus on high-quality images. Through the
            use of responsive images and compression techniques, the size of the
            webpage was reduced from 40MB to about 4MB on mobile. This allowed
            the start render time to fall below the sacred 3-seconds-maximum
            rule while on regular 4G speeds.
          </Piece>
          <Piece
            image="bloxels-edu-map"
            suffix="png"
            alt="Screenshot of map showing where bloxels is used in the classroom"
          >
            I really enjoyed working with Pixel Press since they were so open
            and available with me. This communication allowed for a quality
            product to be developed and{' '}
            <strong>delivered in under three weeks time</strong> from start to
            finish. This was my second project with them and they liked my work
            well enough to hire me again to work on the design for their{' '}
            <a href="/bloxels-shopify">Shopify site</a>.
          </Piece>
        </main>
      </div>
    );
  }
}

export default BloxelsEdu;
