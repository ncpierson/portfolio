import { h, Component } from 'preact';

import Hero from '../../components/hero';
import Piece from '../../components/piece';

const roles = ['development', 'design', 'hosting'];

class SoundOfText extends Component {
  render() {
    return (
      <div id="#soundoftext">
        <Hero
          title="Sound of Text"
          image="soundoftext"
          suffix="png"
          client="myself"
          roles={roles}
        />
        <main class="project">
          <Piece
            image="soundoftext2014"
            suffix="png"
            label="2014"
            alt="Screenshot of Sound of Text in 2014; designed terribly"
            center
          >
            <p>
              <a
                href="https://soundoftext.com"
                target="_blank"
                rel="noreferrer"
              >
                Sound of Text
              </a>{' '}
              is a text-to-speech service backed by Google Translate. Both a
              website and an API are available. Google Translate itself does not
              provide the option to download the MP3 files for its audio clips,
              and Sound of Text makes that possible. It is not technically
              sophisticated, however there is a huge demand for this service and
              I created Sound of Text to fill that role.
            </p>
            <p>
              On the first day it launched, Sound of Text received almost 500
              users visiting the site. Shortly after, it kept a steady pace
              growing from 50 users per day to now reaching{' '}
              <strong>ten thousand users per day</strong>. Almost 10GB of MP3
              files are created every month on Sound of Text.
            </p>
          </Piece>
          <Piece
            image="soundoftext2015"
            suffix="png"
            label="2015"
            alt="Screenshot of Sound of Text in 2015; designed well"
            center
            reverse
          >
            <p>
              The tech stack behind Sound of Text is <strong>React</strong> for
              the front-end, and <strong>Express</strong>, <strong>Node</strong>,
              and <strong>MongoDB</strong> behind the API. When a word or phrase
              is requested, the server will fetch the MP3 file from Google
              Translate and copy it to <strong>DigitalOcean Spaces</strong>{' '}
              immediately. If a duplicate request comes through, the MP3 file is
              fetched from the object storage instead of making a request to
              Google Translate. This allows me to be a better web citizen and
              reduce my impact on Google's servers.
            </p>
            <p>
              I designed the Sound of Text website myself. It was important to
              be <strong>lightweight and mobile-friendly</strong> since nearly
              85% of my user base is on a mobile device. The design for Sound of
              Text is also intentionally simple in order to introduce the
              minimum amount of shock to veteran users of the site.
            </p>
          </Piece>
          <Piece
            image="soundoftext"
            suffix="png"
            label="Today"
            alt="Screenshot of Sound of Text today; designed amazingly"
          >
            <p>
              I am responsible for all of the tech infrastructure as well.
              Despite the occasional spike to 200 concurrent users, both the
              website and API are still hosted together on the{' '}
              <strong>smallest droplet available on DigitalOcean</strong> with
              no performance issues. I have Automated Deployment set up through
              Gitlab CI.
            </p>
            <p>
              Sound of Text is my primary hobby project and it has been a blast
              to grow along with it ever since 2014. It is run by donations and
              typically makes just enough to cover hosting costs. Even after
              three years it is still growing steadily and I am looking forward
              to serving even more users.
            </p>
          </Piece>
          <Quote
            authorName="Alex Andrews"
            authorDesc={
              <span>
                Founder and Developer at{' '}
                <a href="http://www.tenkettles.com">Ten Kettles</a>
              </span>
            }
          >
            Two often overlooked qualities of a good developer are communication
            and care. As a daily user of Nick's Sound of Text web app, I can say
            that Nick has these qualities in abundance. He genuinely welcomes
            feedback, implements changes quickly, and provides handy software
            too!
          </Quote>
        </main>
      </div>
    );
  }
}

function Quote(props) {
  return (
    <div class="quote">
      <blockquote class="quote__text">"{props.children}"</blockquote>
      <div class="quote__author-name">{props.authorName}</div>
      <div class="quote__author-desc">{props.authorDesc}</div>
    </div>
  );
}

export default SoundOfText;
