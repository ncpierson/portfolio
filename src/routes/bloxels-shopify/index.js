import { h, Component } from 'preact';

import Hero from '../../components/hero';
import Piece from '../../components/piece';

const roles = ['development'];

class BloxelsShopify extends Component {
  render() {
    return (
      <div id="#bloxels-shopify">
        <Hero
          title="Bloxels - Shopify"
          image="bloxels-shopify"
          suffix="png"
          client="Pixel Press"
          roles={roles}
        />
        <main class="project">
          <Piece image="bloxels-shopify" suffix="png" alt="">
            I was contracted by{' '}
            <a href="http://www.projectpixelpress.com">Pixel Press</a> to build
            a theme for{' '}
            <a href="http://bloxels.myshopify.com">their Shopify store</a>. They
            wanted their store front to match the design of the rest of their
            online presence.{' '}
            <strong>I had not previously worked with Shopify before</strong>,
            but I was willing to to take the plunge.
          </Piece>
          <Piece image="bloxels-shopify-product" suffix="png" alt="" reverse>
            <p>
              I was excited that a client wanted to hire me to work with a
              platform that I had never worked with before. This gave me the
              opportunity to{' '}
              <strong>learn something new and grow my skill set</strong>. It
              took about a week of working with the Shopify platform before I
              was really comfortable with it and able to understand everything
              that it had to offer.
            </p>
            <p>
              I ended up using the Shopify Slate theme in order to get the
              project started. This allowed the usage of{' '}
              <strong>Liquid templates and SCSS</strong>. Where JavaScript was
              needed, I used <strong>Backbone.js</strong>.
            </p>
          </Piece>
          <Piece image="bloxels-shopify-cart" suffix="png" alt="">
            <p>
              <strong>
                Communication with the Pixel Press team was important
              </strong>{' '}
              during this project. Neither the client nor I knew the full extent
              of the requirements to build a Shopify theme. This project was
              going to be a learning experience for the both of us. There were
              times when newfound information impacted the current designs or
              created new dependencies.
            </p>
            <p>
              Thanks to <strong>constant communication</strong> in Slack, there
              were hardly any breaks in productivity. Afterwards, it was also
              critical to share what I had learned about Shopify in order to
              make the hand-off as smooth as possible and to ensure that there
              were no hurdles for future maintenance.
            </p>
          </Piece>
        </main>
      </div>
    );
  }
}

export default BloxelsShopify;
