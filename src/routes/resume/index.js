import { h, Component } from 'preact';

import style from './style';

class Resume extends Component {
  render() {
    return (
      <div id={style.resume}>
        <div class={style.paper}>
          <header class={style.header}>
            <h1 class={style.header__title}>Nick Pierson</h1>
            <ul class={style.header__contact}>
              <li>nick@nickpierson.name</li>
              <li>
                <a href="https://nickpierson.name">nickpierson.name</a>
              </li>
            </ul>
          </header>
          <main>
            <section class={style.section}>
              <Header>Experience</Header>
              <div class={style.section__body}>
                <Experience
                  title="Full-Stack Web Developer"
                  company="Self Employed"
                  time="Mar 2017 to Dec 2017"
                >
                  <li>
                    Scaled a personal web application to 20,000 daily users.
                  </li>
                  <li>
                    Partnered with local startups to fulfill their custom web
                    development needs.
                  </li>
                  <li>
                    Managed personal time and business finances to run a
                    sustainable business.
                  </li>
                </Experience>
                <Experience
                  title="Software Engineer"
                  company="Asynchrony Labs"
                  time="Aug 2015 to Feb 2017"
                >
                  <li>
                    Built a Master Data Management solution using Scala, Play,
                    Kafka, and Neo4j.
                  </li>
                  <li>
                    Worked with Agile teams practicing TDD, pair programming,
                    and kanban.
                  </li>
                </Experience>
                <Experience
                  title="Software Engineer, Intern"
                  company="Union Pacific"
                  time="May 2014 to Aug 2014"
                >
                  <li>
                    Replaced an expensive, technical process with a
                    database-driven, user-friendly web application using Java,
                    Spring, and jQuery.
                  </li>
                  <li>Automated an email alert system stylized using Ink.</li>
                </Experience>
                <Experience
                  title="Software Engineer, Intern"
                  company="Asynchrony Labs"
                  time="May 2013 to Aug 2013"
                >
                  <li>
                    Delivered a Q&amp;A submission and voting platform for
                    Android in an Agile, pair-programming, and test-driven
                    environment.
                  </li>
                </Experience>
              </div>
            </section>
            <section class={style.section}>
              <Header>Skills</Header>
              <div class={style.section__body}>
                <div class={style.grid}>
                  <ul class={`${style.skills} ${style.grid__item}`}>
                    <li>Node / JS</li>
                    <li>Scala / Play</li>
                    <li>React</li>
                  </ul>
                  <ul class={`${style.skills} ${style.grid__item}`}>
                    <li>Ruby / Rails</li>
                    <li>SQL / NoSQL</li>
                    <li>Responsive Web Design</li>
                  </ul>
                  <ul class={`${style.skills} ${style.grid__item}`}>
                    <li>Respect</li>
                    <li>Kindess</li>
                    <li>Empathy</li>
                  </ul>
                </div>
              </div>
            </section>
            <section class={style.section}>
              <Header>Education</Header>
              <div class={style.section__body}>
                <Experience
                  title="B.S. in Computer Science"
                  company="University of Missouri, GPA 3.687"
                  time="Fall 2012 to Spring 2015"
                />
              </div>
            </section>
            <section class={style.section}>
              <Header>Projects</Header>
              <div class={style.section__body}>
                <div class={style.grid}>
                  <Project
                    title="Sound of Text"
                    link="soundoftext.com"
                    href="https://soundoftext.com"
                  >
                    A modern, responsive web application which lets users
                    download TTS MP3 files for many different languages. Used by
                    language learners around the world — attracting 20,000
                    visitors per day. Uses the Google Translate API.
                  </Project>
                  <Project
                    title="KC Photography"
                    link="kaitlynclink.com"
                    href="http://kaitlynclink.com"
                  >
                    A website that I built for my cousin to showcase her
                    photography work and attract clients while she grows her
                    photography business. Designed, developed, and hosted by me
                    using plain old html and css.
                  </Project>
                  <Project
                    title="Pouch"
                    link="pouch.party"
                    href="http://pouch.party"
                  >
                    A meant-for-fun web application that allows you to view your
                    bank account balance in terms of gold, silver, and copper —
                    inspired by World of Warcraft. A work in progress.
                  </Project>
                </div>
              </div>
            </section>
          </main>
        </div>
      </div>
    );
  }
}

function Header(props) {
  return (
    <div class={style.section__header}>
      <h2>{props.children}</h2>
    </div>
  );
}

function Experience(props) {
  return (
    <div class={style.experience}>
      <h3 class={style.experience__title}>{props.title}</h3>
      <div class={style.experience__meta}>
        {props.company && (
          <div class={style.experience__company}>{props.company}</div>
        )}
        <div class={style.experience__time}>{props.time}</div>
      </div>
      {props.children.length > 0 && (
        <ul class={style.experience__text}>{props.children}</ul>
      )}
    </div>
  );
}

function Project(props) {
  return (
    <div class={`${style.project} ${style.grid__item}`}>
      <h3 class={style.project__title}>{props.title}</h3>
      <a class={style.project__link} href={props.href}>
        {props.link}
      </a>
      <p class={style.project__text}>{props.children}</p>
    </div>
  );
}

export default Resume;
